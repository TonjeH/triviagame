export const difficulties = [
  {
    id: 0,
    name: "Choose a difficulty",
  },
  {
    id: 1,
    name: "Easy",
  },
  {
    id: 2,
    name: "Medium",
  },
  {
    id: 3,
    name: "Hard",
  },
]