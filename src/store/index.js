import { createUser, fetchUsers } from '../api/triviaAPI';
import router from '../routes';
import { createStore } from 'vuex';
import { fetchCategories, fetchQuestions } from '../api/opentdbAPI';
import { difficulties } from '../lib/difficulty';

export default createStore({
  state:  {
    username: '',
    userId: 0,
    highscore: 0,
    currentScore: 0,
    amountQuestions: 10,
    category: 0,
    categories: [],
    difficulty: '',
    difficulties: difficulties,
    questions: [],
    questionId: 0,
    answers: [],
  },
  
  mutations: {
    setUsername: (state, payload) => {
      state.username = payload;
    },
    setUserId: (state, payload) => {
      state.userId = payload;
    },
    setHighscore: (state, payload) => {
      state.highscore = payload;
    },
    setCurrentScore: (state, payload) => {
      state.currentScore = payload;
    },
    setAmountQuestions: (state, payload) => {
      state.amountQuestions = payload;
    },
    setCategory: (state, payload) => {
      state.category = payload;
    },
    setCategories: (state, payload) => {
      state.categories = payload;
    },
    setDifficulty: (state, payload) => {
      state.difficulty = payload;
    },
    setDifficulties: (state, payload) => {
      state.difficulties = payload;
    },
    setQuestions: (state, payload) => {
      state.questions = payload;
    },
    setQuestionId: (state, payload) => {
      state.questionId = payload;
    },
    setAnswers: (state, payload) => {
      state.answers = payload;
    },
  },

  actions: {
    async fetchCategoriesAtInit({ commit }) {
      const { trivia_categories } = await fetchCategories();
      commit('setCategories', trivia_categories);
    },
    
    onTextChangeHandler({ commit }, {target: { value: username}}) {
      commit('setUsername', username)
    },

    onNumberChangeHandler({ commit }, { target: { value: amountQuestions }}) {
      commit('setAmountQuestions', amountQuestions);
    },

    async onCategoryChangeHandler({ commit }, { target: { value: categoryId }}) {
      commit('setCategory', categoryId);
    },
    
    onDifficultyChangeHandler({ commit }, { target: { value: difficulty }}) {
      commit('setDifficulty', difficulty);
    },
    
    async fetchQuestions({ commit, state }) {
      const { questions } = await fetchQuestions(state.amountQuestions, state.category, state.difficulty);
      commit('setQuestions', questions)
    },

    async onStartClickHandler({ commit, state }) {
      let user = await fetchUsers(state.username);
      console.log("User: ", user);
      if(user.length === 0) {
        user = await createUser(state.username, 0);
      }
      commit('setUserId', user.id);
      commit('setHighscore', user.highscore);
      commit('setCurrentScore', 0);

      const { questions } = await fetchQuestions(state.amountQuestions, state.category, state.difficulty);
      commit('setQuestions', questions)

      await router.push('/question');
    }
  },
  
});

