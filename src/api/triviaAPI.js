const triviaAPIUrl = 'https://trivia-game-api-assignment.herokuapp.com';
const apiKey = 'FoKUN9hfEvhLxtm2374Z8FEr';

export async function fetchUsers(username) {
  try {
    console.log("Fetching users...")
    return await fetch(`${triviaAPIUrl}/trivia/?username=${username}`).then(res => res.json());
  } catch(err) {
    console.log("Error: ", err.message);
  }
}

export async function createUser(username, highscore) {
  try {
    return await fetch(`${triviaAPIUrl}/trivia`, {
      method: 'POST',
      headers: {
        'X-API-Key': apiKey,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: username,
        highscore: highscore
      })
    })
    .then(res => {
      if(!res.ok) {
        throw new Error('Could not create new user')
      }
      return res.json();
    })
  } catch(err) {
    console.log("Error: ", err.message);
  }
}

export async function updateHighscore(userId, highscore) {
  try{
    return await fetch(`${triviaAPIUrl}/trivia/${userId}`, {
      method: 'PATCH',
      headers: {
        'X-API-Key': apiKey,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        highscore: highscore
      })
    })
    .then(res => {
    if(!res.ok) {
      throw new Error('Could not update highscore')
    }
    return res.json();
    });
  } catch(err) {
    console.log("Error: ", err.message);
  }
}