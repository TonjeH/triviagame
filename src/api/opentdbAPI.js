import { difficulties } from '../lib/difficulty';
import { triviaAPIUrl } from '../../config';
import { triviaAPICategoriesUrl } from '../../config';

export async function fetchQuestions(amountQuestions = 10, category, difficultyId) {
  try {
    let url = triviaAPIUrl;
    url += `amount=${amountQuestions}`
    
    if(category != 0) {
      url += `&category=${category}`;  
    }
    if(difficultyId !== 0) {
      const difficulty = difficulties[difficultyId].name.toLowerCase();
      url += `&difficulty=${difficulty}`;
    }

    console.log("Test: Current url for fetching: ", url)

    return await fetch(url)
      .then((q) => q.json())
  } catch (err) {
    console.log("Error: ", err.message);
  } 
}

export async function fetchCategories() {
  try {
    return await fetch(triviaAPICategoriesUrl)
      .then((res) => res.json());
  } catch(err) {
    console.log("Error: ", err.message);
  }
}