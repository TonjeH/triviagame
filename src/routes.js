import { createRouter, createWebHistory } from 'vue-router';
import StartPage from '@/components/StartPage';
import QuestionPage from '@/components/QuestionPage';
import ResultPage from '@/components/ResultPage';

const routes = [
  {
    path: '/',
    name: 'Start Page',
    component: StartPage
  }, {
    path: '/question',
    name: 'Question Page',
    component: QuestionPage
  }, {
    path: '/result',
    name: 'Result',
    component: ResultPage
  }
]
const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router;